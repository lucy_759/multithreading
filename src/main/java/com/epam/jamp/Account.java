package com.epam.jamp;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;

public class Account {
    private int balance;
    private Lock lock;
    private AtomicInteger failCounter;

    public Account(int balance) {
        this.balance = balance;
    }

    public Account(int balance, Lock lock) {
        this.balance = balance;
        this.lock = lock;
    }

    public Account(int balance, Lock lock, AtomicInteger failCounter) {
        this.balance = balance;
        this.lock = lock;
        this.failCounter = failCounter;
    }

    public void withdraw(int amount) {
        balance -= amount;
    }

    public void deposit(int amount) {
        balance += amount;
    }

    public void incFailedTransferCount() {
        failCounter.incrementAndGet();
    }

    public int getBalance() {
        return balance;
    }

    public Lock getLock() {
        return lock;
    }

    public int getFailCounter() {
        return failCounter.get();
    }
}
