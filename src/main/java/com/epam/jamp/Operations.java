package com.epam.jamp;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class Operations {

    public static final int W8_TIME = 2;

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch startLatch = new CountDownLatch(1);
        final Account a = new Account(1000, new ReentrantLock(), new AtomicInteger(0));
        final Account b = new Account(2000, new ReentrantLock(), new AtomicInteger(0));
//        //
//        new Thread(() -> {
//            try {
//                transfer(a, b, 500);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }).start();
//        //        transfer(a, b, 500);
//        transfer(b, a, 300);
//        System.out.println(a.getBalance());
//        System.out.println(b.getBalance());
//
//        new Thread(() -> {
//            try {
//                transferWithLock(a, b, 500, 2);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }).start();
//        //        transfer(a, b, 500);
//        transferWithLock(b, a, 300, 3);
//        System.out.println(a.getBalance());
//        System.out.println(b.getBalance());
//        System.out.println(a.getFailCounter());
//        System.out.println(b.getFailCounter());

        ExecutorService service = Executors.newFixedThreadPool(3);
        List<Future<Boolean>> futures = new ArrayList<>();

        Random random = new Random();
        int totalAmountOfTransferedMoney = 0;
        for (int i = 0; i < 10; i++) {
            int i1 = random.nextInt(400);
            totalAmountOfTransferedMoney += i1;
            Transfer transfer = new Transfer(b, a, i1, startLatch);
            futures.add(service.submit(transfer));
        }

        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(3);
        scheduledExecutorService.scheduleAtFixedRate(() -> System.out.println("First acc fail trys - " + a.getFailCounter()), 0,500, TimeUnit.MILLISECONDS);
        scheduledExecutorService.scheduleAtFixedRate(() -> System.out.println("Second acc fail trys - " + b.getFailCounter()), 0, 500, TimeUnit.MILLISECONDS);;
        service.shutdown();
        startLatch.countDown();
        boolean result = service.awaitTermination(60, TimeUnit.SECONDS);
        if (result) {
            System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAA");
        }
        //        futures.forEach(booleanFuture -> {
//            try {
//                booleanFuture.get();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            } catch (ExecutionException e) {
//                e.printStackTrace();
//            }
//        });

        // W8 till transfers from first account would finish, only then transfer from second account.
        Account firstAccount = new Account(1000);
        Account secondAccount = new Account(1000);

        int numberOfF2STransactions = 3;
        int numberOfS2FTransactions = 4;


        CountDownLatch startingLatch = new CountDownLatch(1);
        CountDownLatch waitingLatch = new CountDownLatch(numberOfS2FTransactions);

        ExecutorService cachedExecutor = Executors.newCachedThreadPool();

        for (int i = numberOfF2STransactions; i > 0; i--) {
            cachedExecutor.submit(new Transfer(firstAccount, secondAccount, random.nextInt(400), startLatch, waitingLatch));
        }

        for (int i = numberOfS2FTransactions; i > 0; i--) {
            cachedExecutor.submit(new Transfer(secondAccount, firstAccount, random.nextInt(400), startingLatch));
        }

        cachedExecutor.shutdown();
        startingLatch.countDown();

        boolean waitExpec = service.awaitTermination(
                20, TimeUnit.SECONDS);

        if (waitExpec) {
            System.out.println("Finished succesfully.");
        }
    }

    public static void transfer(Account fAccount, Account sAccount, int ammount)
            throws InterruptedException {
        if (fAccount.getBalance() < ammount) {
            throw new IllegalArgumentException("Insufficient funds");
        }
        synchronized (fAccount) {
            //            Thread.sleep(1000);
            synchronized (sAccount) {
                fAccount.withdraw(ammount);
                sAccount.deposit(ammount);
            }
        }
    }

    public static void transferWithLock(Account fAccount, Account sAccount, int ammount, int w8Time)
            throws InterruptedException {
        if (fAccount.getLock().tryLock(w8Time, TimeUnit.SECONDS)) {
            try {
                Thread.sleep(1000);
                if (sAccount.getLock().tryLock(w8Time, TimeUnit.SECONDS)) {
                    try {
                        fAccount.withdraw(ammount);
                        sAccount.deposit(ammount);
                    } finally {
                        sAccount.getLock().unlock();
                    }
                } else {
                    System.out.println("second acc is locket for a long time");
                    sAccount.incFailedTransferCount();
                }
            } finally {
                fAccount.getLock().unlock();
            }
        } else {
            System.out.println("first acc is locket for a long time");
            fAccount.incFailedTransferCount();
        }
    }
}
