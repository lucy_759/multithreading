package com.epam.jamp;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class OperationsCompletableFuture {



    public static void main(String[] args) {
        final Account accountFrom = new Account(1000, new ReentrantLock(), new AtomicInteger(0));
        final Account b = new Account(2000, new ReentrantLock(), new AtomicInteger(0));

        CompletableFuture<Integer> futureAmmount =
                CompletableFuture.supplyAsync(OperationsCompletableFuture::requestTransferAmount);
        CompletableFuture<Account> accountCompletableFuture = CompletableFuture
                .supplyAsync(OperationsCompletableFuture::requestDestinationAccount);
        CompletableFuture<Void> futureTransfer = futureAmmount.thenCombineAsync(accountCompletableFuture,
                (amount, accountTo) -> createTransfer(accountFrom, accountTo, amount))
                .thenApply(Transfer::perform)
                .exceptionally(OperationsCompletableFuture::processTransferExceotion)
                .thenAcceptAsync(OperationsCompletableFuture::processTransferResult);
    }

    private static int requestTransferAmount() {
        return 10;
    }

    private static Account requestDestinationAccount() {
        return new Account(2000, new ReentrantLock(), new AtomicInteger(0));
    }

    private static Transfer createTransfer(Account a1, Account a2, int amt) {
        return new Transfer(a1, a2, amt);
    }

    private static boolean processTransferExceotion(Throwable throwable) {
        return Objects.nonNull(throwable);
    }

    private static void processTransferResult(boolean b) {
        String status = "succesfully";
        if (!b) {
            status = "un" + status;
        }
        System.out.println("Transfer operation finished " + status);
    }
}
