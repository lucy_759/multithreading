package com.epam.jamp;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class Transfer implements Callable<Boolean> {

    private static final int LOCK_WAIT_SEC = 3;
    private static final int MAX_TRANSFER_SEC = 4;

    private Account accountFrom;
    private Account accountTo;
    private int amount;
    private CountDownLatch startLatch;
    private CountDownLatch waitingLatch;


    private final Random waitRandom = new Random();

    public Transfer(Account accountFrom, Account accountTo, int amount, CountDownLatch startLatch,
            CountDownLatch waitingLatch) {
        this.accountFrom = accountFrom;
        this.accountTo = accountTo;
        this.amount = amount;
    }

    public Transfer(Account b, Account a, int i1, CountDownLatch latch) {
        this.accountFrom = accountFrom;
        this.accountTo = accountTo;
        this.amount = amount;
        this.waitingLatch = latch;
    }

    public Transfer(Account accountFrom, Account accountTo, int amount) {
        this.accountFrom = accountFrom;
        this.accountTo = accountTo;
        this.amount = amount;
    }

    public Boolean call() throws Exception {
        if (waitingLatch != null) {
            waitingLatch.await();
        }
        if (accountFrom.getLock().tryLock(LOCK_WAIT_SEC, TimeUnit.SECONDS)) {
            try {
                if (accountFrom.getBalance() < amount) {
                    accountFrom.incFailedTransferCount();
                    throw new IllegalStateException("Insufficient funds in Account " + accountFrom);
                }

                if (accountFrom.getLock().tryLock(LOCK_WAIT_SEC, TimeUnit.SECONDS)) {
                    try {

                        accountFrom.withdraw(amount);
                        accountFrom.deposit(amount);

                        Thread.sleep(waitRandom.nextInt(MAX_TRANSFER_SEC * 1000));

                        System.out.println("Transfer " + amount + " done.");

                        return true;

                    } finally {
                        accountFrom.getLock().unlock();
                        if (waitingLatch != null) {
                            waitingLatch.countDown();
                        }
                    }
                } else {
                    accountFrom.incFailedTransferCount();
                    return false;
                }
            } finally {
                accountFrom.getLock().unlock();
            }
        } else {
            accountFrom.incFailedTransferCount();
            return false;
        }
    }

    public Boolean perform() {
        try {
            return call();
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

}
